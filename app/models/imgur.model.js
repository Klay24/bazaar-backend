module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          title: String,
          createdAt: Date
        }
      );
      
      schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      });

      const Imgur = mongoose.model("imgur2", schema);
      return Imgur;
  };