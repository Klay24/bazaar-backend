module.exports = mongoose => {
    const schema = mongoose.Schema({
        title: String,
        price: Number,
        category: String,
        subCategory: String,
        brand: String,
        params:String,
        description: [],
        originalUrl: String,
        picUrl: [],
        createdAt: Date,
        updatedAt: Date
    });

    const Product = mongoose.model("products", schema);
    return Product;
};