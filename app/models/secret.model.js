module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          title: String,
          subCategory: [],
          createdAt: Date,
          updatedAt: Date
        }
      );
    
      const Secret = mongoose.model("category", schema);
      return Secret;
  };