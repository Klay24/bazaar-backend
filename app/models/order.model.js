const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

module.exports = mongoose => {
    const schema = mongoose.Schema({
        memberId: ObjectId,
        name: String,
        address: String,
        phone: String,
        products: [],
        createdAt: Date,
        updatedAt: Date,
    });

    const Order = mongoose.model("orders", schema);
    return Order;
};