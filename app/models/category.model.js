module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
          title: String,
          subCategory: [],
          params: String,
          order: Number,
          createdAt: Date,
          updatedAt: Date,
        }
      );
    
      const Category = mongoose.model("categories", schema);
      return Category;
  };