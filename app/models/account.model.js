module.exports = mongoose => {
    const schema = mongoose.Schema({
        name: String,
        email: String,
        password: String,
        loginType: String,
        cart:[],
        address: [],
        orders: [],
        createdAt: Date,
        updatedAt: Date,
    });

    const Account = mongoose.model("accounts", schema);
    return Account;
};