// const dbConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = process.env.MONGO_DB_URL;
db.tutorials = require("./tutorial.model.js")(mongoose);
db.imgur = require("./imgur.model.js")(mongoose);
db.secret = require("./secret.model.js")(mongoose);
db.category = require("./category.model")(mongoose);
db.product = require("./product.model")(mongoose);
db.account = require("./account.model")(mongoose)
db.order = require("./order.model")(mongoose)


module.exports = db;