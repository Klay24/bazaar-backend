const db = require("../models")
const Order = db.order
const Account = db.account
const Product = db.product
const myDate = require('../script/date')
const assert = require('assert')
const { json } = require("body-parser")
const ObjectId = db.mongoose.Types.ObjectId
// const axios = require("axios");

exports.CreateOrder = async (req, res) => {
    const time = myDate.getDate()
    const info = req.body
    const session = await db.mongoose.startSession();
    session.startTransaction();

    try {
        const account = await Account.findById(info.memberId, null, {
            session: session
        })
        assert.ok(account.$session)

        const order = new Order({
            memberId: ObjectId(info.memberId),
            name: info.name,
            address: info.address,
            products: account.cart,
            phone: info.phone,
            prods: account.cart,
            createdAt: time,
            updatedAt: time
        })
        
        const doc = await Order.create([order], {
            session: session
        })
        assert.ok(doc)

        if (account.address.indexOf(info.address) === -1) {
            account.address.push(info.address)
        }

        account.orders.push(doc[0]._id)
        account.cart = []
        await account.save()

        await session.commitTransaction();
        res.send({
            success: true,
            message: ''
        })
    } catch (error) {
        await session.abortTransaction();
        res.status(500).send({
            success: false,
            message: error.message
        })
    } finally {
        session.endSession();
    }
}

exports.GetHistoryOrders = async (req, res) => {
    const account = await Account.findById(req.params._id)
    let orderInfo = await Order.aggregate([{
        $match: {
            memberId: account._id
        }
    },{
        $sort:{createdAt:-1},
        
    }
])

    for (let i = 0; i < orderInfo.length; i++) {
        const order = orderInfo[i];
        order.productsInfo =[]
        for (let j = 0; j < order.products.length; j++) {
            const product = order.products[j];
            let productInfo = await Product.findById(product._id)
            let copyInfo = JSON.parse(JSON.stringify(productInfo))
            copyInfo.quantity = product.quantity
            order.productsInfo.push(copyInfo)
        }
    }
    // const orderInfo = await Order.aggregate([{
    //     $match: {
    //         memberId: account._id
    //     }
    // }, {
    //     $lookup: {
    //         from: 'accounts',
    //         localField: 'memberId',
    //         foreignField: '_id',
    //         as: 'memberData'
    //     }
    // }])
    res.send(orderInfo)
}