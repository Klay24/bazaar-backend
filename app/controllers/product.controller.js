const db = require("../models")
const Product = db.product
const myDate = require('../script/date')
let data = require("../json/bathBody.json")
const {
    get
} = require("mongoose")
const { product } = require("../models")

exports.GetProductsByCategory = ((req, res) => {
    const subCategory = req.params.subCategory
    
    let condition = {
        category: req.params.category,
    }
    
    if (subCategory !== 'all') {
        condition.subCategory = subCategory
    }
    Product.find(condition).then(data => {
        res.send(data)
    }).catch(err => {
        res.status(500).send({
            message: `error : ${err.message}`
        })
    })
})

exports.GetProductByName = ((req, res) => {
    const productName = req.params.productName
    Product.findOne({
        params: productName
    }).then(data => {
        res.send(data)
    }).catch(err => {
        res.status(500).send({
            message: `error : ${err.message}`
        })
    })
})

exports.GetAllCartItems = (async (req, res) => {
    const productsId = req.body.productsId
    const result = []

    for (let index = 0; index < productsId.length; index++) {
        const id = productsId[index];
        const product = await Product.findById(id)
        result.push(product)
    }

    res.send(result)
})

exports.updateFromJson = ((req, res) => {
    data.forEach(element => {
        let parse = parseInt(element.price)
        element.price = parse ? parse : element.price
        element.category = 'bath-body'
        element.subCategory = ''
        const time = myDate.getDate()
        element.createdAt = time
        element.updatedAt = time
    })

    Product.insertMany(data).then(
        d => {
            res.send({
                message: 'ok'
            })
        }).catch(err => {
        res.status(500).send({
            message: `error : ${err.message}`
        })
    })
})