const db = require("../models");
const Secret = db.secret;
const category = require('../json/category.json')

// Create and Save a new Tutorial
exports.insert = (req, res) => {

    let dt = new Date()
    dt.setHours(dt.getHours() + 8)
    category.forEach(element => {
        element.createdAt = dt
        element.updatedAt = dt
    });

    // Save Tutorial in the database
    Secret
        .insertMany(category)
        .then(data => {
            res.send({message: 'success'});
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Tutorial."
            });
        });
};