const db = require("../models")
const Account = db.account
const axios = require("axios");
const myDate = require('../script/date')
const ObjectId = db.mongoose.Types.ObjectId
// const RedirectUrl = 'http://localhost:3000'
const RedirectUrl = 'https://klay24.gitlab.io/bazzar/'


exports.CreateAccount = (req, res) => {
    const info = req.body
    const time = myDate.getDate()

    const account = new Account({
        name: info.name,
        email: info.email,
        password: info.password,
        loginType: 'Original',
        createdAt: time,
        updatedAt: time
    })

    account.save().then(data => res.send({
        message: 'create account success'
    })).catch(err => res.status(500).send({
        message: err.message || "Some error occurred while creating the Tutorial."
    }))
}

exports.OriginalLogin = async (req, res) => {
    const condition = {
        email: req.query.email
    }
    const info = await Account.findOne(condition)

    if (!info) {
        res.send({
            message: 'not found'
        })
        return
    }

    if (info.password !== req.query.password) {
        res.send({
            message: 'password error'
        })
        return
    }

    res.send({
        message: 'login',
        token: "fake token",
        _id: info._id
    })
}

exports.GoogleLogin = async (req, res) => {
    //get id token
    const
        data = await axios({
            url: `https://oauth2.googleapis.com/token`,
            method: 'post',
            data: {
                client_id: '435073218601-p2jpokl54gub8235838gdgi69hhucc28.apps.googleusercontent.com',
                client_secret: 'Btpb-LLG1qfLdghGMHGP_kH2',
                redirect_uri: `${RedirectUrl}/login/confirm`,
                grant_type: 'authorization_code',
                code: req.body.code,
            },
        }).catch(err => console.log(err))

    //get member info
    const googleInfo = await axios.get(`https://oauth2.googleapis.com/tokeninfo?id_token=${data.data.id_token}`)
    const check = await Account.findOne({
        email: googleInfo.data.email
    })

    const result = {
        message: 'login',
        token: "fake token"
    }

    if (!check) {
        const time = myDate.getDate()

        const account = new Account({
            name: googleInfo.data.name,
            email: googleInfo.data.email,
            password: '',
            loginType: 'Google',
            createdAt: time,
            updatedAt: time
        })
        const createAccount = await account.save()
        result._id = createAccount._id
    }else{
        result._id = check._id
    }

    res.send(result)
}

exports.saveCart = async (req, res) => {
    const time = myDate.getDate()
    let cart = req.body.cart
    const memberId = req.body._id
    const currAccount = await Account.findById(memberId)
    currAccount.updatedAt = time

    if (!req.body.modified) {
        cart = cart.map(x => {
            for (let i = 0; i < currAccount.cart.length; i++) {
                const item = currAccount.cart[i]

                if (item.skip) {
                    continue
                }

                if (item._id.toString() === x._id) {
                    item.skip = true
                    return {
                        _id: ObjectId(x._id),
                        quantity: x.quantity + item.quantity
                    }
                }
            }
            return {
                _id: ObjectId(x._id),
                quantity: x.quantity
            }
        })

        for (let index = 0; index < currAccount.cart.length; index++) {
            const element = currAccount.cart[index];
            if (!element.skip) {
                cart.push(element)
            }
        }
    }

    currAccount.cart = cart
    await currAccount.save()
    res.send({
        success: true,
        message: ''
    })
}

//after member login, update cart with new product
exports.UpdateCart = async (req,res) =>{
    const id = req.body._id
    const cart = req.body.cart
    const account = await Account.findById(id)
    account.cart = cart
    await account.save()
    res.send({
        success:true,
        message:''
    })
}

exports.GetCartByMember = async (req, res) => {
    const _id = req.query._id
    const currAccount = await Account.findById(_id)
    res.send(currAccount.cart)
}

exports.test = (req, res) => {
    res.status(405).send({
        message: 'test'
    })
}