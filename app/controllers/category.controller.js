const db = require("../models")
const Category = db.category

// Get all category info
exports.GetCategory = (req, res) => {
    Category.find({}).then(data => {

        data.sort((a, b) => {
            if (a.order > b.order) return 1
            if (a.order < b.order) return -1
            return 0
        })

        const result = []

        data.forEach(element => {
            result.push({
                _id: element._id,
                title: element.title,
                subCategory: element.subCategory
            })
        })
        res.send(result)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving GetCategory."
        })
    })
}

//get information of a category
exports.GetOneCategory = (req, res) => {
    Category.findOne({
        params: req.params.category
    }).then(data =>
        res.send({
            title: data.title,
            subCategory: data.subCategory
        })
    )
}

exports.test = (req, res) => {
    res.status(200).send({
        message: 'test'
    })
}