module.exports = app => {
    const category = require("../controllers/category.controller");
  
    var router = require("express").Router();

    router.get('/', category.GetCategory)

    router.get('/:category', category.GetOneCategory)

    app.use('/api/category', router);
  };