module.exports = app => {
    const order = require("../controllers/order.controller");

    var router = require("express").Router();

    router.post('/', order.CreateOrder)

    router.get('/:_id', order.GetHistoryOrders)

    app.use('/api/order', router);
  };