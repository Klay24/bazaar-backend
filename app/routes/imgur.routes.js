module.exports = app => {
    const imgur = require("../controllers/imgur.controller.js");
  
    var router = require("express").Router();

    router.post('/upload', imgur.upload)

    router.get('/test', imgur.test)

    app.use('/api/imgur', router);
  };