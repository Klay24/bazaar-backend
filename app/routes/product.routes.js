module.exports = app => {
    const product = require("../controllers/product.controller");
  
    var router = require("express").Router();

    router.get('/:category/:subCategory', product.GetProductsByCategory)

    router.get('/:productName', product.GetProductByName)

    router.post('/cart', product.GetAllCartItems)

    router.get('/update', product.updateFromJson)

    app.use('/api/product', router);
  };