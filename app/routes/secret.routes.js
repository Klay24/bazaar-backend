module.exports = app => {
    const secret = require("../controllers/secret.controller");
  
    var router = require("express").Router();

    router.get('/', secret.insert)

    app.use('/api/secret', router);
  };