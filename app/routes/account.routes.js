module.exports = app => {
    const account = require("../controllers/account.controller");

    var router = require("express").Router();

    router.post('/google-login', account.GoogleLogin)

    router.post('/', account.CreateAccount)

    router.get('/original-login', account.OriginalLogin)
    
    router.get('/cart', account.GetCartByMember)

    router.patch('/save-cart', account.saveCart)

    router.patch('/update-cart', account.UpdateCart)

    router.get('/test', account.test)

    app.use('/api/account', router);
  };