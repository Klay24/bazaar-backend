FROM node:lts-alpine

WORKDIR /usr/app

COPY . .

RUN npm install

EXPOSE 8080

# ARG MONGO_DB_URL

# ENV MONGO_DB_URL=${MONGO_DB_URL}

CMD npm run start

# docker build --build-arg MONGO_DB_URL=[url] -t bazaar-backend .
# docker run -p 8080:8080 -e "MONGO_DB_URL=[url]" --name bazaar-backend-container bazaar-backend 
# docker run -p 8080:8080 --env-file=.env --name bazaar-backend-container bazaar-backend